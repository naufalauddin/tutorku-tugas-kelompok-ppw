from django.shortcuts import render

def hello(request):
    return render(request, 'hello_world.html')

def template(request):
    return render(request, 'template.html')

def home(request):
    return render(request, 'accounts/home.html')
