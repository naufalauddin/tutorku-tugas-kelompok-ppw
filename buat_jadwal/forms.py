from django import forms
from .models import Jadwal

class BuatJadwalForms(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = ['hari', 'matkul', 'jam_mulai', 'jam_selesai']
        widgets = {
            'hari': forms.Select(
            ),
            'jam_mulai': forms.TimeInput(
                attrs={
                    'type':'time'
                }
            ),
            'jam_selesai': forms.TimeInput(
                attrs={
                    'type':'time'
                }
            ),
            'is_hidden':False
        }
    
