from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.contrib.auth.decorators import login_required

from .forms import BuatJadwalForms
from .models import Jadwal

# Create your views here.
# @login_required
# @tutor_required
def buat(request):
    if request.method == 'POST':
        form = BuatJadwalForms(request.POST)
        form.tutor = request.user
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('homepage'))
    else:
        form = BuatJadwalForms()
    return render(
        request,
        'buat_jadwal/buat_jadwal.html',
        {
            'form': form,
        }
    )

def berhasil(request):
    return render(request, 'buat_jadwal/berhasil.html')

def list_jadwal(request):
    jadwals = Jadwal.objects.order_by('hari')
    return render(request, 'buat_jadwal/list.html', {
        'jadwals': jadwals
    })
