from django import forms

class SearchForm(forms.ModelForm):
	class Meta:
		matkul = forms.CharField(max_length=50)
		hari = forms.CharField(max_length=50)
