from django.db import models
from django.contrib.auth import get_user_model
from buat_jadwal.models import Jadwal
# Create your models here.


class RelasiJadwal(models.Model):

    tutee = models.ForeignKey(
	    get_user_model(),
	    on_delete=models.CASCADE,
	    null=True,
	    blank=True,
        )
    
    jadwal = models.ForeignKey(
	    Jadwal,
	    on_delete=models.CASCADE,
	    null=True,
	    blank=True,
        )

	
